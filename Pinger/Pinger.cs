﻿using System;
using System.Threading.Tasks;
using RabbitMQ.Wrapper;

namespace Pinger
{
    class Pinger
    {
        private static readonly string hostName = "localhost";
        private static readonly string sendingQueue = "pong_queue";
        private static readonly string listeningQueue = "ping_queue";
        private static readonly string exchange = "pingpong";
        private static readonly string routingKey = "ping";
        private static readonly string message = "ping";
        private static readonly int millisecondsDelay = 2500;
        public static void Main()
        {
            Console.WriteLine("PINGER, ctrl+c to exit");
            using (var rabbitClient = new RabbitClient(hostName, sendingQueue, listeningQueue, exchange, routingKey))
            {
                while (true)
                {
                    if (rabbitClient.SendMessageToQueue(message))
                    {
                        Console.WriteLine(" [X] Sent {0} at {1}", message, DateTime.Now);
                    }
                    else
                    {
                        Console.WriteLine(" [X] Message not sent. Termination");
                        break;
                    }
                    Console.WriteLine(" [X] Got {0} at {1}", rabbitClient.ListenQueue().Result, DateTime.Now);
                    Task.Delay(millisecondsDelay).Wait();
                }
            }
        }
    }
}
