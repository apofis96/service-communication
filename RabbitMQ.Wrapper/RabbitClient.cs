﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Client.Exceptions;
using System;
using System.Text;
using System.Threading.Tasks;

namespace RabbitMQ.Wrapper
{
    public class RabbitClient: IDisposable
    {
        private readonly IConnection _connection;
        private TaskCompletionSource<string> tcs;
        private readonly string _listeningQueue;
        private readonly string _exchange;
        private readonly string _routingKey;
        public RabbitClient(string hostName, string sendingQueue, string listeningQueue, string exchange, string routingKey)
        {
            _listeningQueue = listeningQueue;
            _exchange = exchange;
            _routingKey = routingKey;
            var factory = new ConnectionFactory() { HostName = hostName };
            _connection = factory.CreateConnection();
            using (var channel = _connection.CreateModel())
            {
                channel.ExchangeDeclare(exchange: exchange, type: "direct");
                channel.QueueDeclare(queue: sendingQueue, durable: false, exclusive: false, autoDelete: false, arguments: null);
                channel.QueueDeclare(queue: listeningQueue, durable: false, exclusive: false, autoDelete: false, arguments: null);
                channel.QueueBind(queue: sendingQueue, exchange: exchange, routingKey: routingKey);
                channel.BasicQos(0, 1, true);
            }
        }
        public Task<string> ListenQueue()
        {
            var channel = _connection.CreateModel();
            tcs = new TaskCompletionSource<string>();
            channel.BasicQos(0, 1, true);
            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += (model, ea) =>
            {
                var response = Encoding.UTF8.GetString(ea.Body.ToArray());
                if (tcs.TrySetResult(response))
                {
                    channel.BasicAck(deliveryTag: ea.DeliveryTag, multiple: false);
                }
                else
                {
                    channel.BasicNack(deliveryTag: ea.DeliveryTag, multiple: false, requeue: true);
                }
                channel.Close();
            };
            channel.BasicConsume(
                consumer: consumer,
                queue: _listeningQueue,
                autoAck: false);
            return tcs.Task;
        }
        public bool SendMessageToQueue(string message)
        {
            try
            {
                using (var channel = _connection.CreateModel())
                {
                    var body = Encoding.UTF8.GetBytes(message);
                    channel.ConfirmSelect();
                    channel.BasicPublish(exchange: _exchange, routingKey: _routingKey, basicProperties: null, body: body);
                    channel.WaitForConfirmsOrDie(new TimeSpan(0, 0, 3));
                    return true;
                }
            }
            catch (OperationInterruptedException)
            {
                return false;
            }
        }
        public void Dispose()
        {
            _connection?.Dispose();
        }
    }
}